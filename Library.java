/**
 * Program Perpustakaan
 * @author Dwiki Herdiansyah (201943500183)
 * @version 1.0.0
 * Menu:
 * - Buku
 * -- Lihat buku
 * -- Tambah buku
 * -- Ubah buku
 * -- Hapus buku
 * 
 * - Peminjaman buku
 * -- Buat/tambah peminjaman buku
 * 
 * - Pengembalian buku
 * -- Pencatatan/input pengembalian buku yang di pinjam
 * 
 * - Riwayat
 * -- Riwayat buku yang masih di pinjam
 * -- Riwayat buku yang sudah di kembalikan
 * -- Riwayat buku
 */

// package
import global.driver.Database;
import global.driver.Query;
import global.helper.Table;
import global.helper.Table.EnumAlignment;
import global.helper.Common;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
public class Library
{
    // global variable
    private static Database db;
    private static Common common;
    private static Query query;
    private static String curr_page = "home";
    private static String prev_page = "";
    
    /**
     * Fungsi constructor
     */
    public Library()
    {
        try {
            db = new Database("library.db");
            common = new Common();
            query = new Query(db.connect());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } 
    }

    /**
     * Index
     */
    public static void main(String[] args)
    {
        new Library();
        //pages(curr_page);
        Map<String, String> where = new HashMap<String, String>();
        where.put("kode_buku", "001");
        System.out.println(where.isEmpty());
        for (Map.Entry<String, String> entry : where.entrySet()) {
            System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
        }
        /*query
        .select()
        .get("t_buku");*/
    }
    
    /**
     * Fungsi pindah halaman
     * @param String page
     * @return view
     */
    private static void pages(String page)
    {        
        switch(page) {
            case "home":
            case "back":
                curr_page = page;
                homePage();
            break;
            
            case "book":
                curr_page = page;
                bookPage();
            break;
            
            case "brrw":
                curr_page = page;
                borrowPage();
            break;
            
            case "rtrn":
                curr_page = page;
                returnPage();
            break;
            
            case "hstr":
                curr_page = page;
                historyPage();
            break;
            
            case "exit":
                exitPage();
            break;
            
            default:
                curr_page = (curr_page != null) ? curr_page : "home";
                common.addNotification("Kode tidak valid", "");
                pages(curr_page);
        }
    }
    
    /**
     * Fungsi pilih kode
     */
    private static void chooseCode()
    {
        Scanner input = new Scanner(System.in);
        String pilih;
        
        System.out.print("Kode: "); pilih = input.next();
        
        pilih = pilih.toLowerCase();
        pages(pilih);
    }
    
    /**
     * Halaman utama
     */
    private static void homePage() {
        common.clrscr();
        common.notification();
        
        System.out.println("PROGRAM PERPUSTAKAAN|HAMALAN UTAMA");
        
        Table tbl = new Table(2, "Kode", "Menu").withSpacing(2);
        tbl.addRow("HOME", "Halaman Utama");
        tbl.addRow("BOOK", "Data Buku");
        tbl.addRow("BRRW", "Peminjaman Buku");
        tbl.addRow("RTRN", "Pengembalian Buku");
        tbl.addRow("HSTR", "Riwayat");
        tbl.addRow("EXIT", "Tutup Program");
        tbl.print();
        
        chooseCode();
    }
    
    /**
     * Halaman data buku
     */
    private static void bookPage() {
        common.clrscr();
        common.notification();
        
        System.out.println("PROGRAM PERPUSTAKAAN|DATA BUKU");
        
        Table tbl = new Table(2, "Kode", "Menu").withSpacing(2);
        tbl.addRow("BOOK-LST", "Lihat Data Buku");
        tbl.addRow("BOOK-ADD", "Tambah Data Buku");
        tbl.addRow("BOOK-UPD", "Ubah Data Buku");
        tbl.addRow("BOOK-DEL", "Hapus Data Buku");
        tbl.addRow("BACK", "Kembali ke Halamn Utama");
        tbl.print();
        
        chooseCode();
    }
    
    /**
     * Halaman peminjaman buku
     */
    private static void borrowPage() {
        common.clrscr();
        common.notification();
        
        System.out.println("PROGRAM PERPUSTAKAAN|PEMINJAMAN BUKU");
        
        Table tbl = new Table(2, "Kode", "Menu").withSpacing(2);
        tbl.addRow("BRRW-ADD", "Tambah Peminjaman Buku");
        tbl.addRow("BACK", "Kembali ke Halamn Utama");
        tbl.print();
        
        chooseCode();
    }
    
    /**
     * Halaman pengembalian buku
     */
    private static void returnPage() {
        common.clrscr();
        common.notification();
        
        System.out.println("PROGRAM PERPUSTAKAAN|PENGEMBALIAN BUKU");
        
        Table tbl = new Table(2, "Kode", "Menu").withSpacing(2);
        tbl.addRow("RTRN-INPT", "Input Pengembalian Buku");
        tbl.addRow("BACK", "Kembali ke Halamn Utama");
        tbl.print();
        
        chooseCode();
    }
    
    /**
     * Halaman riwayat
     */
    private static void historyPage() {
        common.clrscr();
        common.notification();
        
        System.out.println("PROGRAM PERPUSTAKAAN|RIWAYAT");
        
        Table tbl = new Table(2, "Kode", "Menu").withSpacing(2);
        tbl.addRow("HSTR-BRRW", "Riwayat Peminjaman Buku");
        tbl.addRow("HSTR-RTRN", "Riwayat Buku Yang Sudah Dikembalikan");
        tbl.addRow("HSTR-BOOK", "Riwayat Buku");
        tbl.addRow("BACK", "Kembali ke Halamn Utama");
        tbl.print();
        
        chooseCode();
    }
    
    /**
     * Fungsi tutup program
     */
    private static void exitPage() {
        common.clrscr();
        
        System.out.println("Menutup program perpustakaan..");
        db.disconnect();
        System.exit(0);
    }
}
