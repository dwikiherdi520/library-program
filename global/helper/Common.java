package global.helper;

import global.helper.FontColor;
public class Common
{
    private String notif;
    private String type_status = "info";

    /**
     * Constructor for objects of class Common
     */
    public Common()
    {
        this.notif = null;
    }
    
    /**
     * Clear screen function
     */
    public void clrscr()
    {
        // clear screen
        System.out.print('\u000C');
    }
    
    /**
     * Add message function
     * @param String msg
     * @param String type
     * @return this
     */
    public Common addNotification(String msg, String type)
    {
        this.notif = msg;
        this.type_status = type;
        return this;
    }
    
    /**
     * Remove message function
     * @return this
     */
    public Common removeNotification()
    {
        this.notif = null;
        this.type_status = "info";
        return this;
    }
    
    /**
     * Show message function
     * @return this
     */
    public Common notification()
    {
        if (this.notif != null)
        {
            String fcolor;
            switch(this.type_status) {
              case "info":
                fcolor = FontColor.BLUE_BRIGHT;
                break;
              case "success":
                fcolor = FontColor.GREEN_BRIGHT;
                break;
              case "warning":
                fcolor = FontColor.YELLOW;
                break;
              case "error":
                fcolor = FontColor.RED;
                break;
              default:
                fcolor = FontColor.RESET;
            }
            //System.out.println(fcolor+"Notifikasi: "+this.notif+FontColor.RESET);
            //System.err.println("Notifikasi: "+this.notif);
            System.out.println("Notifikasi: "+this.notif);
            System.out.println();
            removeNotification();
        }
        return this;
    }

}
