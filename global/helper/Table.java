/**
 * Package from
 * https://github.com/2xsaiko/crogamp/blob/master/src/com/github/mrebhan/crogamp/cli/TableList.java
 */

package global.helper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Pattern;

public class Table {

	private static final String BLINE = "-";
	private static final String CROSSING = "+";
	private static final String VERTICAL_TSEP = "|";
	private static final String VERTICAL_BSEP = "|";

	private String[] descriptions;
	private ArrayList<String[]> table;
	private int[] tableSizes;
	private int rows;
	private int findex;
	private String filter;
	private Comparator<String[]> comparator;
	private int spacing;
	private EnumAlignment aligns[];

	public Table(String... descriptions) {
		this(descriptions.length, descriptions);
	}
	
	public Table(int columns, String... descriptions) {
		if (descriptions.length != columns) {
			throw new IllegalArgumentException();
		}
		this.filter = null;
		this.rows = columns;
		this.descriptions = descriptions;
		this.table = new ArrayList<>();
		this.tableSizes = new int[columns];
		this.updateSizes(descriptions);
		this.spacing = 1;
		this.aligns = new EnumAlignment[columns];
		this.comparator = null;
		for (int i = 0; i < aligns.length; i++) {
			aligns[i] = EnumAlignment.LEFT;
		}
	}

	private void updateSizes(String[] elements) {
		for (int i = 0; i < tableSizes.length; i++) {
			if (elements[i] != null) {
				int j = tableSizes[i];
				j = Math.max(j, elements[i].length());
				tableSizes[i] = j;
			}
		}
	}

	public Table compareWith(Comparator<String[]> c) {
		this.comparator = c;
		return this;
	}

	public Table sortBy(int column) {
		return this.compareWith((o1, o2) -> o1[column].compareTo(o2[column]));
	}

	public Table align(int column, EnumAlignment align) {
		aligns[column] = align;
		return this;
	}

	public Table withSpacing(int spacing) {
		this.spacing = spacing;
		return this;
	}

	/**
	 * Adds a row to the table with the specified elements.
	 */

	public Table addRow(String... elements) {
		if (elements.length != rows) {
			throw new IllegalArgumentException();
		}
		table.add(elements);
		updateSizes(elements);
		return this;
	}

	public Table filterBy(int par0, String pattern) {
		this.findex = par0;
		this.filter = pattern;
		return this;
	}

	public void print() {
		StringBuilder line = null;

		// print vertical seperator
		for (int i = 0; i < rows; i++) {
			if (line != null) {
				line.append(CROSSING);
			} else {
				line = new StringBuilder();
				line.append(CROSSING);
			}

			for (int j = 0; j < tableSizes[i] + 2 * spacing; j++) {
		        if ((j+1) == (tableSizes[i] + 2 * spacing) && ((i+1) == rows)) {
		            line.append(CROSSING);
		        } else {
		            line.append(BLINE);
		        }
			}
		}
		System.out.println(line.toString());

		// print header
		line = null;
		for (int i = 0; i < rows; i++) {
			if (line != null) {
				line.append(VERTICAL_TSEP);
			} else {
				line = new StringBuilder();
				line.append(VERTICAL_TSEP);
			}
			String part = descriptions[i];
			while (part.length() < tableSizes[i] + spacing) {
				if ((part.length()+1) == (tableSizes[i] + spacing) && ((i+1) == rows)) {
					part += VERTICAL_TSEP;
				} else {
					part += " ";
				}
			}
			for (int j = 0; j < spacing; j++) {
				line.append(" ");
			}
			line.append(part);
		}
		System.out.println(line.toString());

		// print vertical seperator
		line = null;
		for (int i = 0; i < rows; i++) {
			if (line != null) {
				line.append(CROSSING);
			} else {
				line = new StringBuilder();
				line.append(CROSSING);
			}
			for (int j = 0; j < tableSizes[i] + 2 * spacing; j++) {
		        if ((j+1) == (tableSizes[i] + 2 * spacing) && ((i+1) == rows)) {
		            line.append(CROSSING);
		        } else {
		            line.append(BLINE);
		        }
			}
		}
		System.out.println(line.toString());

		// print add row
		if (table.isEmpty() == false) {
			line = null;
			ArrayList<String[]> localTable = table;

			if (filter != null) {
				Pattern p = Pattern.compile(filter);
				localTable.removeIf(arr -> {
					String s = arr[findex];
					return !p.matcher(s).matches();
				});
			}

			if (localTable.isEmpty()) {
				String[] sa = new String[rows];
				localTable.add(sa);
			}

			localTable.forEach(arr -> {
				for (int i = 0; i < arr.length; i++) {
					if (arr[i] == null) {
						arr[i] = "";
					}
				}
			});

			if (comparator != null) {
				localTable.sort(comparator);
			}

			for (String[] strings : localTable) {
				for (int i = 0; i < rows; i++) {
					if (line != null) {
						line.append(VERTICAL_BSEP);
					} else {
						line = new StringBuilder();
						line.append(VERTICAL_BSEP);
					}
					String part = "";
					for (int j = 0; j < spacing; j++) {
						part += " ";
					}
					if (strings[i] != null) {
						switch (aligns[i]) {
						case LEFT:
							part += strings[i];
							break;
						case RIGHT:
							for (int j = 0; j < tableSizes[i] - strings[i].length(); j++) {
								part += " ";
							}
							part += strings[i];
							break;
						case CENTER:
							for (int j = 0; j < (tableSizes[i] - strings[i].length()) / 2; j++) {
								part += " ";
							}
							part += strings[i];
							break;
						}
					}
					while (part.length() < tableSizes[i] + spacing) {
						part += " ";
					}
					for (int j = 0; j < spacing; j++) {
						if (((j+1) == spacing) && ((i+1) == rows)) {
							part += VERTICAL_BSEP;
						} else {
							part += " ";
						}
					}
					line.append(part);
				}
				System.out.println(line.toString());
				line = null;
			}
		}

		// print vertical seperator
		line = null;
		for (int i = 0; i < rows; i++) {
			if (line != null) {
				line.append(CROSSING);
			} else {
				line = new StringBuilder();
				line.append(CROSSING);
			}
			for (int j = 0; j < tableSizes[i] + 2 * spacing; j++) {
		        if ((j+1) == (tableSizes[i] + 2 * spacing) && ((i+1) == rows)) {
		            line.append(CROSSING);
		        } else {
		            line.append(BLINE);
		        }
			}
		}
		System.out.println(line.toString());
	}

	public static enum EnumAlignment {
		LEFT, CENTER, RIGHT
	}

}