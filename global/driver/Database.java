package global.driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import global.driver.Migration;
public class Database
{
    private static Connection db;
    /**
     * Constructor for objects of class Database
     * @param database name
     */
    public Database(String db_name) throws Exception
    {
       Class.forName("org.sqlite.JDBC");
       try {
           db = DriverManager.getConnection("jdbc:sqlite:"+db_name);
       } catch (SQLException e) {
           System.out.println(e.getMessage());
       } finally {
           migration();
       }
    }

    /**
     * Connect to Database
     * return Connection
     */
    public static Connection connect() 
    {
       return db;
    }
    
    /**
     * Disconnect database
     * return Connection
     */
    public static void disconnect()
    {
        try {
            if (db != null) {
                db.close();
                db = null;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Migration Table database
     * return null
     */
    private static void migration() throws Exception
    {
        new Migration(db);
    }
}
