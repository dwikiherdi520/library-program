package global.driver;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class Migration
{
    private static Connection db;
    /**
     * Constructor for objects of class Migration
     */
    public Migration(Connection database)
    {
        db = database;
        tableBuku();
    }
    
    /**
     * Check table database is exists or not
     * @param String tb_name
     * @return boolean
     */
    protected static boolean checkExistTable(String tb_name)
    {
        boolean ret = false;
        try {
            DatabaseMetaData dbm = db.getMetaData();
            ResultSet tbl = dbm.getTables(null, null, tb_name, null);
            if (tbl.next()) {
                ret = true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }
    
    /**
     * check column table is exists or not
     * @param String tb_name
     * @param String col_name
     * @return boolean
     */
    protected static boolean checkExistColumn(String tb_name, String col_name)
    {
        boolean ret = false;
        try {
            DatabaseMetaData dbm = db.getMetaData();
            ResultSet col = dbm.getColumns(null, null, tb_name, col_name);
            if (col.next()) {
                ret = true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }
    
    protected static void tableBuku()
    {
        String table = "t_buku";
        String sql   = "";
        try {
            if (checkExistTable(table) == false) {
                sql += "create table "+table+"("
                    +   "id integer primary key autoincrement not null,"
                    +   "kode_buku varchar(10) not null,"
                    +   "nama_buku varchar(100) default null,"
                    +   "penerbit varchar(100) default null,"
                    +   "stock integer default 0"
                    + ");";
            }
            
            if (sql != "") {
                Statement stat = db.createStatement();
                stat.executeUpdate(sql);
                stat.close();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
