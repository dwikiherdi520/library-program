package global.driver;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
public class Query
{
    // global variables
    private static Connection db;
    private static String[] select_query;

    /**
     * Constructor for objects of class Query
     */
    public Query(Connection database)
    {
        this.db = database;
        this.select_query = new String[] {};
    }
    
    public Query select(String... select_query) {
        this.select_query = select_query;
        return this;
    }
    
    public void get(String table_name)
    {
        String select =  (this.select_query.length == 0) ? "*" : String.join(", ", this.select_query);
        System.out.println(
                   "select " +select+ 
                   " from " +table_name+ 
               ";");
    }
    
    public void getArray(String table_name)
    {
        
    }
    
    public void getRow()
    {
        
    }
}
